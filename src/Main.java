import com.zuitt.example.Car;
import com.zuitt.example.Dog;

public class Main {
    public static void main(String[] args) {

        Car myCar = new Car();
        myCar.drive();
        System.out.println(myCar.getDriverName());
        myCar.setBrand("Kia");
        myCar.setName("Sorrento");
        myCar.setYear_make(2022);
        System.out.println("The " + myCar.getBrand() + " " + myCar.getName() + " " + myCar.getYear_make() + " was driven by " + myCar.getDriverName());

        // another Car
        Car yourCar = new Car();
        System.out.println("Your car was driven by: " + myCar.getDriverName());

        // Animal and Dog
        Dog myPet = new Dog("Brownie","White","Any breed");
        myPet.setName("Brownie");
        myPet.setColor("White");
        myPet.speak();
        System.out.println(myPet.getName() + " " + myPet.getColor() + " " + myPet.getBreed());
    }
}